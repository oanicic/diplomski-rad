﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ahycoV3.Models;
using System.IO;
using Microsoft.AspNet.Identity;
using PagedList;

namespace ahycoV3.Controllers
{
    public class ClanaksController : Controller
    {


        private ahycoV2Entities db = new ahycoV2Entities();

        // GET: Clanaks
        public ActionResult Index(string sortOrder, int? page, long? katId)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.UserSortParm = sortOrder == "User" ? "user_desc" : "User";
            var clanaks = (from b in db.Clanaks where b.kategorijaId == katId select b);

            switch (sortOrder)
            {
                case "name_desc":
                    clanaks = clanaks.OrderByDescending(s => s.naslovClanak);
                    break;
                case "Date":
                    clanaks = clanaks.OrderBy(s => s.datumClanak);
                    break;
                case "date_desc":
                    clanaks = clanaks.OrderByDescending(s => s.datumClanak);
                    break;
                case "User":
                    clanaks = clanaks.OrderBy(s => s.AspNetUser.Email);
                    break;
                case "user_desc":
                    clanaks = clanaks.OrderByDescending(s => s.AspNetUser.Email);
                    break;
                default:
                    clanaks = clanaks.OrderBy(s => s.naslovClanak);
                    break;
            }


            foreach (var clanak in clanaks)
            {
                string myString = clanak.sadrzajClanak.Substring(0, 200);
                int index = myString.LastIndexOf(' ');
                string outputString = myString.Substring(0, index);
                clanak.sadrzajClanak = outputString + "...";
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.kategorijaId = katId;
            return View(clanaks.ToPagedList(pageNumber, pageSize));
        }



        // GET: Clanaks
        public ActionResult IndexUser(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.UserSortParm = sortOrder == "User" ? "user_desc" : "User";

            var clanaks = from s in db.Clanaks.Include(c => c.AspNetUser).Include(c => c.Kategorije) select s;

            var CurrentUserId = User.Identity.GetUserId(); ;

            if (this.User.IsInRole("Editor"))
            {
                clanaks = (from b in db.Clanaks
                           where b.userId == CurrentUserId
                           select b);
            }

            switch (sortOrder)
            {
                case "name_desc":
                    clanaks = clanaks.OrderByDescending(s => s.naslovClanak);
                    break;
                case "Date":
                    clanaks = clanaks.OrderBy(s => s.datumClanak);
                    break;
                case "date_desc":
                    clanaks = clanaks.OrderByDescending(s => s.datumClanak);
                    break;
                case "User":
                    clanaks = clanaks.OrderBy(s => s.AspNetUser.Email);
                    break;
                case "user_desc":
                    clanaks = clanaks.OrderByDescending(s => s.AspNetUser.Email);
                    break;
                default:
                    clanaks = clanaks.OrderBy(s => s.naslovClanak);
                    break;
            }
            foreach (var clanak in clanaks)
            {
                string myString = clanak.sadrzajClanak.Substring(0, 200);
                int index = myString.LastIndexOf(' ');
                string outputString = myString.Substring(0, index);
                clanak.sadrzajClanak = outputString + "...";
            }
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            return View(clanaks.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult PregledKategorija()
        {
            return View(db.Kategorijes.ToList());
        }

        public ActionResult PregledClanakaPoKategorijama(long katId)
        {
            var clanaks = from s in db.Clanaks select s;
            clanaks = (from b in db.Clanaks
                       where b.kategorijaId == katId
                       select b).Take(3);
            
            return PartialView("_prikazClanakaPoKat", clanaks.ToList());
        }

        // GET: Clanaks/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clanak clanak = db.Clanaks.Find(id);
            if (clanak == null)
            {
                return HttpNotFound();
            }
            ViewBag.istiUser = false;
            if (User.Identity.GetUserId() == clanak.AspNetUser.Id)
            {
                ViewBag.istiUser = true;
            }


            return View(clanak);
        }

        // GET: Clanaks/Create
        [Authorize(Roles = "Administrator, Editor")]
        public ActionResult Create()
        {
            ViewBag.userId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.kategorijaId = new SelectList(db.Kategorijes, "kategorijaId", "nazivKategorija");
            return View();
        }

        // POST: Clanaks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Editor")]
        public ActionResult Create([Bind(Include = "clanakId,naslovClanak,sadrzajClanak,naslovnaClanak,datumClanak,kategorijaId,userId,uvodClanak")] Clanak clanak)
        {
            if (ModelState.IsValid)
            {
                byte[] imageData = null;
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase poImgFile = Request.Files["ClanakNaslovna"];
                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        imageData = binary.ReadBytes(poImgFile.ContentLength);
                        clanak.naslovnaClanak = imageData;
                    }
                }

                if (this.User.IsInRole("Editor"))
                {
                    clanak.userId = User.Identity.GetUserId();
                }

                db.Clanaks.Add(clanak);
                db.SaveChanges();
                return RedirectToAction("Details/" + clanak.clanakId.ToString());
            }
            ViewBag.userId = new SelectList(db.AspNetUsers, "Id", "Email", clanak.userId);
            ViewBag.kategorijaId = new SelectList(db.Kategorijes, "kategorijaId", "nazivKategorija", clanak.kategorijaId);
            return View(clanak);
        }

        // GET: Clanaks/Edit/5
        [Authorize(Roles = "Administrator, Editor")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clanak clanak = db.Clanaks.Find(id);
            if (clanak == null)
            {
                return HttpNotFound();
            }
            ViewBag.userId = new SelectList(db.AspNetUsers, "Id", "Email", clanak.userId);
            ViewBag.kategorijaId = new SelectList(db.Kategorijes, "kategorijaId", "nazivKategorija", clanak.kategorijaId);
            return View(clanak);
        }

        // POST: Clanaks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit([Bind(Include = "clanakId,naslovClanak,sadrzajClanak,naslovnaClanak,datumClanak,kategorijaId,userId,uvodClanak")] Clanak clanak)
        {

            if (ModelState.IsValid)
            {
                byte[] imageData = null;
                if (Request.Files["ClanakNaslovna"] != null)
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpPostedFileBase poImgFile = Request.Files["ClanakNaslovna"];
                        using (var binary = new BinaryReader(poImgFile.InputStream))
                        {
                            imageData = binary.ReadBytes(poImgFile.ContentLength);
                            clanak.naslovnaClanak = imageData;
                        }
                    }
                }
                db.Entry(clanak).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexUser");
            }
            ViewBag.userId = new SelectList(db.AspNetUsers, "Id", "Email", clanak.userId);
            ViewBag.kategorijaId = new SelectList(db.Kategorijes, "kategorijaId", "nazivKategorija", clanak.kategorijaId);
            return View(clanak);
        }

        // GET: Clanaks/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clanak clanak = db.Clanaks.Find(id);
            if (clanak == null)
            {
                return HttpNotFound();
            }
            return View(clanak);
        }

        // POST: Clanaks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(long id)
        {
            Clanak clanak = db.Clanaks.Find(id);
            db.Clanaks.Remove(clanak);
            db.SaveChanges();
            return RedirectToAction("IndexUser");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
