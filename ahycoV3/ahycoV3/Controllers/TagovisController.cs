﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ahycoV3.Models;

namespace ahycoV3.Controllers
{
    public class TagovisController : Controller
    {
        private ahycoV2Entities db = new ahycoV2Entities();

        // GET: Tagovis
        public ActionResult Index()
        {
            return View(db.Tagovis.ToList());
        }

        // GET: Tagovis/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tagovi tagovi = db.Tagovis.Find(id);
            if (tagovi == null)
            {
                return HttpNotFound();
            }
            return View(tagovi);
        }

        // GET: Tagovis/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tagovis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tagId,nazivTag")] Tagovi tagovi)
        {
            if (ModelState.IsValid)
            {
                db.Tagovis.Add(tagovi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tagovi);
        }

        // GET: Tagovis/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tagovi tagovi = db.Tagovis.Find(id);
            if (tagovi == null)
            {
                return HttpNotFound();
            }
            return View(tagovi);
        }

        // POST: Tagovis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tagId,nazivTag")] Tagovi tagovi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tagovi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tagovi);
        }

        // GET: Tagovis/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tagovi tagovi = db.Tagovis.Find(id);
            if (tagovi == null)
            {
                return HttpNotFound();
            }
            return View(tagovi);
        }

        // POST: Tagovis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Tagovi tagovi = db.Tagovis.Find(id);
            db.Tagovis.Remove(tagovi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
