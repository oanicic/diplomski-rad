﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ahycoV3.Models;
using System.IO;

namespace ahycoV3.Controllers
{
    public class KategorijesController : Controller
    {
        private ahycoV2Entities db = new ahycoV2Entities();

        // GET: Kategorijes
        public ActionResult Index()
        {
            return View(db.Kategorijes.ToList());
        }

        public ActionResult NaslovOpisKategorija(long katId)
        {
            var kategorije = from s in db.Kategorijes select s;
            kategorije = from b in db.Kategorijes
                         where b.kategorijaId == katId
                         select b;
            return PartialView("_naslovOpisKategorija", kategorije.ToList());
        }

        // GET: Kategorijes/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategorije kategorije = db.Kategorijes.Find(id);
            if (kategorije == null)
            {
                return HttpNotFound();
            }
            return View(kategorije);
        }

        // GET: Kategorijes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kategorijes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "kategorijaId,nazivKategorija,opisKategorija,slikaKategorija")] Kategorije kategorije)
        {
            if (ModelState.IsValid)
            {
                db.Kategorijes.Add(kategorije);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kategorije);
        }

        // GET: Kategorijes/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategorije kategorije = db.Kategorijes.Find(id);
            if (kategorije == null)
            {
                return HttpNotFound();
            }
            return View(kategorije);
        }

        // POST: Kategorijes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "kategorijaId,nazivKategorija,opisKategorija,slikaKategorija")] Kategorije kategorije)
        {
            if (ModelState.IsValid)
            {

                byte[] imageData = null;
                if (Request.Files["SlikaKategorije"] != null)
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpPostedFileBase poImgFile = Request.Files["SlikaKategorije"];
                        using (var binary = new BinaryReader(poImgFile.InputStream))
                        {
                            imageData = binary.ReadBytes(poImgFile.ContentLength);
                            kategorije.slikaKategorija = imageData;
                        }
                    }
                }

                db.Entry(kategorije).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kategorije);
        }

        // GET: Kategorijes/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategorije kategorije = db.Kategorijes.Find(id);
            if (kategorije == null)
            {
                return HttpNotFound();
            }
            return View(kategorije);
        }

        // POST: Kategorijes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Kategorije kategorije = db.Kategorijes.Find(id);
            db.Kategorijes.Remove(kategorije);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
