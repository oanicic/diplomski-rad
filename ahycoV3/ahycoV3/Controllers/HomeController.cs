﻿using ahycoV3.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ahycoV3.Controllers
{
    public class HomeController : Controller
    {
        private ahycoV2Entities db = new ahycoV2Entities();

        public ActionResult ClanakCarousel()
        {
            Clanak model = new Clanak();

            var clanaks = db.Clanaks.OrderByDescending(t => t.datumClanak).Take(3);
            foreach (var clanak in clanaks)
            {
                string myString = clanak.sadrzajClanak.Substring(0, 200);
                int index = myString.LastIndexOf(' ');
                string outputString = myString.Substring(0, index);
                clanak.sadrzajClanak = outputString + "...";
            }

            return PartialView("_carouselClanak", clanaks.ToList());
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ReturnKategorije()
        {
            return PartialView("_naslovnaKategorije", db.Kategorijes.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public FileContentResult UserPhotos()
        {
            if (User.Identity.IsAuthenticated)
            {
                String userId = User.Identity.GetUserId();

                if (userId == null)
                {
                    string fileName = HttpContext.Server.MapPath(@"~/Content/noImg.png");

                    byte[] imageData = null;
                    FileInfo fileInfo = new FileInfo(fileName);
                    long imageFileLength = fileInfo.Length;
                    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    imageData = br.ReadBytes((int)imageFileLength);

                    return File(imageData, "image/png");

                }
                // to get the user details to load user Image
                var bdUsers = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
                var userImage = bdUsers.Users.Where(x => x.Id == userId).FirstOrDefault();
                if (userImage.Slika == null)
                {
                    string fileName = HttpContext.Server.MapPath(@"~/Content/noImg.png");

                    byte[] imageData = null;
                    FileInfo fileInfo = new FileInfo(fileName);
                    long imageFileLength = fileInfo.Length;
                    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    imageData = br.ReadBytes((int)imageFileLength);
                    return File(imageData, "image/png");
                }
                else
                {
                    return new FileContentResult(userImage.Slika, "image/jpeg");
                }
            }
            else
            {
                string fileName = HttpContext.Server.MapPath(@"~/Content/noImgG.png");

                byte[] imageData = null;
                FileInfo fileInfo = new FileInfo(fileName);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int)imageFileLength);
                return File(imageData, "image/png");

            }
        }
    }
}